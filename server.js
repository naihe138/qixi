const Koa = require('koa')

const static = require('koa-static')
const path = require('path')
const app = new Koa()

app.use(static(__dirname + '/'))

app.listen(3000)